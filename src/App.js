import React, { Component } from 'react';
import './App.css';
//import PropTypes from 'prop-types';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowLabel: 0,
      colLabel: 0,
      longestRowString: 0,
      shortestRowString: 0,
      longestColumnString: 0,
      shortestColumnString: 0,
      imagesUploaded: 0
    };

    this.labelRowArray = [];
    this.labelColumnArray = [];
    this.numberOfImages = [];
    this.appendRow     = this.appendRow.bind(this);
    this.appendColumn  = this.appendColumn.bind(this);
    this.deleteRows    = this.deleteRows.bind(this);
    this.deleteColumns = this.deleteColumns.bind(this);
  }

 appendRow() {
    this.tbl = document.getElementById('my-table');
    this.row = this.tbl.insertRow(this.tbl.rows.length);
    for (let i = 0; i < this.tbl.rows[0].cells.length; i++) {
      this.rowIndex = this.tbl.rows.length-2;
      this.rowLabel = "row"+this.rowIndex ;
      this.labelRowArray.push(this.rowLabel);
      this.longest = this.labelRowArray.reduce((a, b) => a.length > b.length ? a : b, '');
      this.shortest = this.labelRowArray.reduce((a, b) => a.length < b.length ? a : b);
      this.setState({
        rowLabel:this.rowIndex,
        longestRowString:this.longest.length,
        shortestRowString:this.shortest.length
      });
      if (i === 0) {
        this.createButtonCell(this.row.insertCell(),'+', this.rowLabel);
      }
      else if (i === 1) {
        this.createTitleCell(this.row.insertCell(),this.rowLabel, 'row');
      }
      else {
        this.createCell(this.row.insertCell(), 'row');
      }

    }
}

  uploadImage(id) {
    this.numberOfImages.push(id);
    this.unique = [...new Set(this.numberOfImages)];
    this.setState({imagesUploaded: this.unique.length});
    console.log('You have successfully uploaded an image');
  }

 createButtonCell(cell, text, style) {
   let div = document.createElement('input'),
       txt = document.createTextNode(text);
   div.type = "file";
   div.id = style;
   let newlabel = document.createElement("Label");
    newlabel.setAttribute("for",div.id);
    newlabel.innerHTML = newlabel.innerHTML +"+";
   div.addEventListener('click', () => this.uploadImage(div.id), false);
   div.appendChild(txt);
   div.setAttribute('className', style);
   cell.appendChild(newlabel);
   cell.appendChild(div);
}

createTitleCell(cell, text, style) {
   let div = document.createElement('div'),
       txt = document.createTextNode(text);
   div.appendChild(txt);
   div.setAttribute('className', style);
   cell.appendChild(div);
}

createCell(cell, text, style) {
   let div = document.createElement('input'),
       txt = document.createTextNode(text);
   div.type = "radio";
   div.appendChild(txt);
   div.setAttribute('className', style);
   cell.appendChild(div);
}

 appendColumn() {
    this.tbl = document.getElementById('my-table');
    for (let i = 0; i < this.tbl.rows.length; i++) {
      this.colIndex = this.tbl.rows[0].cells.length -2;
      this.colLabel = "Col"+this.colIndex;
      this.labelColumnArray.push(this.colLabel);
      this.longestcol = this.labelColumnArray.reduce((a, b) => a.length > b.length ? a : b, '');
      this.shortestcol = this.labelColumnArray.reduce((a, b) => a.length < b.length ? a : b);
      this.setState({
        colLabel:this.colIndex,
        longestColumnString:this.longestcol.length,
        shortestColumnString:this.shortestcol.length
      });
      if (i === 0) {
        this.createButtonCell(this.tbl.rows[i].insertCell(this.tbl.rows[i].cells.length), '+', this.colLabel);
      }
      else if (i === 1) {
            this.createTitleCell(this.tbl.rows[i].insertCell(this.tbl.rows[i].cells.length), this.colLabel, 'col');
          }
       else {
        this.createCell(this.tbl.rows[i].insertCell(this.tbl.rows[i].cells.length), i, 'col');
      }

    }
}

 deleteRows() {
    this.tbl = document.getElementById('my-table');
    this.lastRow = this.tbl.rows.length - 1;
    for (let i = this.lastRow; i > this.lastRow-1; i--) {
        this.tbl.deleteRow(i);
        if (this.labelRowArray[this.labelRowArray.length-1] === this.labelRowArray[this.labelRowArray.length-2]) {
            this.labelRowArray.splice(-2,2);
        } else {
          this.labelRowArray.splice(-1,1);
        }
        this.longest = this.labelRowArray.reduce((a, b) => a.length > b.length ? a : b, '');
        this.shortest = this.labelRowArray.reduce((a, b) => a.length < b.length ? a : b);
        this.setState({
          rowLabel:this.tbl.rows.length-2,
          longestRowString:this.longest.length,
          shortestRowString:this.shortest.length
        });
    }
}

 deleteColumns() {
    this.tbl = document.getElementById('my-table');
    this.lastCol = this.tbl.rows[0].cells.length - 1;
    for (let i = 0; i < this.tbl.rows.length; i++) {
        for (let j = this.lastCol; j > this.lastCol-1; j--) {
            this.tbl.rows[i].deleteCell(j);
            if (this.labelColumnArray[this.labelColumnArray.length] === this.labelColumnArray[this.labelColumnArray.length-1]) {
              this.labelColumnArray.splice(-2,2);
            } else {
              this.labelColumnArray.splice(-1,1);
            }
            this.longestcol = this.labelColumnArray.reduce((a, b) => a.length > b.length ? a : b, '');
            this.shortestcol = this.labelColumnArray.reduce((a, b) => a.length < b.length ? a : b);
            this.setState({
              colLabel:this.tbl.rows[0].cells.length-2,
              longestColumnString:this.longestcol.length,
              shortestColumnString:this.shortestcol.length
            });
        }
    }
}
  render() {
    return (
      <div className="container-fluid">
        <h2>React Radio-Matrix App</h2>
        <hr/>
       <div className="row">
        <div className="col-sm-9 component-position">
         <div className="component-table">
             <h5>Question Edition View</h5>
             <h6>Title of the question</h6>
          <div className="dl-horizontal-table">
           <table id="my-table"  cellSpacing="0" cellPadding="0" border="0">
            <tbody>
              <tr>
                <td><input type="radio" className="radio-button-display" name="radioMatrix" value="value1"/></td>
                <td><input type="radio" className="radio-button-display" name="radioMatrix" value="value1"/></td>
              </tr>
              <tr>
                <td><input type="radio" className="radio-button-display" name="radioMatrix" value="value1"/></td>
                <td><input type="radio" className="radio-button-display" name="radioMatrix" value="value1"/></td>
              </tr>
            </tbody>
          </table>
            <div className="flex-container-column-plus-minus">
              <button className="flex-button-column-plus-minus" onClick={this.deleteColumns}><span className="glyphicon glyphicon-minus"></span></button>
              <button className="flex-button-column-plus-minus" onClick={this.appendColumn}><span className="glyphicon glyphicon-plus"></span></button>
          </div>
        </div>
          <div className="component-row">
           <div className="flex-container-row-plus-minus pull-left">
             <button className="flex-button-column-plus-minus" onClick={this.deleteRows}><span className="glyphicon glyphicon-minus"></span></button>
             <button className="flex-button-column-plus-minus" onClick={this.appendRow}><span className="glyphicon glyphicon-plus"></span></button>
           </div>
         </div>
        </div>
      </div>
      <div className="col-sm-3">
      <h5>Question Summary View</h5>
      <h6 className="summary">Summary</h6>
         <ul>
          <li>Number of Rows : {this.state.rowLabel}</li>
          <li>Number of Columns : {this.state.colLabel}</li>
          <li>String Length of Longest Row label : {this.state.longestRowString}</li>
          <li>String Length of Shortest Row label : {this.state.shortestRowString} </li>
          <li>String Length of Longest Column label : {this.state.longestColumnString}</li>
          <li>String Length of Shortest Column label : {this.state.shortestColumnString}</li>
          <li>Number of Images Uploaded : {this.state.imagesUploaded}</li>
         </ul>
        </div>
       </div>
      </div>
    );
  }
}

export default App;
