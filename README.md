This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can install node modules by `yarn install` then run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This is the first view after `yarn start`, if you click `+` for row and column you will see new row and column.
![screenshot](screenshots/3.PNG)

After clicking the `+` , you will see the below image.
![screenshot](screenshots/4.PNG)
